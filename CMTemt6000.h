﻿/*
 * CMTemt6000.h
 *
 * Created: 25.02.2017 23:06:00
 *  Author: Christian Möllers
 */ 


#ifndef CMTEMT6000_H_
#define CMTEMT6000_H_

#define workVoltage				3.332 // or 5.0
#define temt6000Resistor		10000.0 // 10 KOhms

//  Temt6000 Pin definition
#define temt6000Sensor			 7
#define temt6000Sensor_PIN       PD7
#define temt6000Sensor_PORT		 PORTD
#define temt6000Sensor_PINR		 PIND
#define temt6000Sensor_OUTPUT    (DDRD |= (1<<temt6000Sensor_PIN))
#define temt6000Sensor_INPUT     (DDRD &=~ (1<<temt6000Sensor_PIN))
#define temt6000Sensor_HI        (PORTD |= (1<<temt6000Sensor_PIN))
#define temt6000Sensor_LOW       (PORTD &=~ (1<<temt6000Sensor_PIN))
#define temt6000Sensor_TOG       (PORTD ^= (1<<temt6000Sensor_PIN))
#define temt6000Sensor_IN        (temt6000Sensor_PINR&(1<<temt6000Sensor_PIN) ? 1 : 0 )

float readTemt6000Sensor(uint8_t adcChannel);

#endif /* CMTEMT6000_H_ */