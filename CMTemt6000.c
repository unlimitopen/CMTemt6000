﻿/*
 * CMTemt6000.c
 *
 * Created: 25.02.2017 23:05:46
 *  Author: Christian Möllers
 */ 

#include <avr/io.h>

#include "../CMGlobal.h"
#include "CMTemt6000.h"
#include "../CMAdcMessure/CMAdcMessure.h"

/**
	@brief	TEMT6000 Sensor, will messure a lux value between 0 - 1000 lux
	@param 	adc channel in digit
	@return	float value in lux
*/
float readTemt6000Sensor(uint8_t adcChannel)
{
	float temt6000Lux = 0.0;
	
	float temt6000AdcValue = adcMessure(adcChannel) * workVoltage / 1024.0;
	
	float temt6000Amps = temt6000AdcValue / temt6000Resistor;
	float temt6000Microamps = temt6000Amps * 1000000;
	temt6000Lux = temt6000Microamps * 2.0;
	
	return temt6000Lux;
}